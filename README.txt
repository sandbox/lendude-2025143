
Views Persistent Click Sort
------------------------
by Len Swaneveld, len@noctilaris.nl


Description
-----------
This module extends the table style plugin provided by Views to include
persistent click sorting on columns. The chosen sort column and directions
are stored in the users session on a view-display basis

Installation 
------------
 * Copy the module's directory to your modules directory and activate the module.
 * A checkbox will be added to the settings page for all table views: 'Use persistent click sort',
 * Check this box if you want to use persistent click sorting for this display/view

