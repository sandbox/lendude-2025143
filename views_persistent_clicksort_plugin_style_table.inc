<?php

/**
 * @file
 * Contains the table style plugin.
 */

/**
 * Style plugin to render each item as a row in a table.
 *
 * @ingroup views_style_plugins
 */
class views_persistent_clicksort_plugin_style_table extends views_plugin_style_table {

  /**
   * Add persistent click sort option
   * @var string
   */
  function option_definition() {
    $options = parent::option_definition();
    $options['persistent_clicksort'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Determine if we should provide sorting based upon $_GET inputs
   * If no new sort order is given in $_GET, see if one is available in $_SESSION
   * If one is found in $_SESSION, set this in $_GET, this will still get run through the
   * sanitation
   *
   * @return bool
   */
  function build_sort() {
    if ($this->options['persistent_clicksort']) {
      $views_key = $this->view->name.'__'.$this->view->current_display;
      if (!isset($_GET['order']) && isset($_SESSION['views_persistent_clicksort'][$views_key]['order'])) $_GET['order'] = $_SESSION['views_persistent_clicksort'][$views_key]['order'];
      else if (isset($_GET['order'])) $_SESSION['views_persistent_clicksort'][$views_key]['order'] = $_GET['order'];
      
      if (!isset($_GET['sort']) && isset($_SESSION['views_persistent_clicksort'][$views_key]['sort'])) $_GET['sort'] = $_SESSION['views_persistent_clicksort'][$views_key]['sort'];
      else if (isset($_GET['sort'])) $_SESSION['views_persistent_clicksort'][$views_key]['sort'] = $_GET['sort'];
    }
    return parent::build_sort();
  }
  
  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['persistent_clicksort'] = array(
      '#title' => t('Use persistent click sort'),
      '#description' => t('The users chosen sort order with be saved in the session and restored if no new sort order was chosen.'),
      '#type' => 'checkbox',
      '#default_value' => $this->options['persistent_clicksort'],
    );
  }
}
